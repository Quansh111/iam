/**
 * Created by master on 01.03.16.
 */
function loadNewItems() {

    // we initiate an xmlhttprequest and read out its body
    xhr("GET","data/listitems.json",null,function(xhr) {
        var textContent = xhr.responseText;
        var jsonContent = JSON.parse(textContent);

        // we assume jsonContent is an array and iterate over its members
        jsonContent.forEach(function(contentItem){
            createListElementForContentItem(contentItem);
        });

    });

}

function loadNewItem() {

    // we initiate an xmlhttprequest and read out its body
    xhr("GET","data/listitems.json",null,function(xhr) {
        var textContent = xhr.responseText;
        var jsonContent = JSON.parse(textContent);
        jsonContent[0].added = (new Date()).toLocaleDateString('de-DE');

        createListElementForContentItem(jsonContent[0]);
    });

}

function createListElementForContentItem(item) {

    var li = document.createElement("li");
        li.classList.add("grid-container");
        var divImg = document.createElement("div");
            li.appendChild(divImg);
            divImg.classList.add("img-list");
            divImg.style.backgroundImage = "url("+item.src+")";
        var divGrau = document.createElement("div");
            li.appendChild(divGrau);
            divGrau.classList.add("grau-text");
            var divUrl = document.createElement("div");
                divGrau.appendChild(divUrl);
                divUrl.textContent = item.owner;
            var divDate = document.createElement("div");
                divGrau.appendChild(divDate);
                divDate.classList.add("date");
                divDate.textContent = item.added;
        var divTitel = document.createElement("div");
            li.appendChild(divTitel);
            divTitel.classList.add("bild-titel");
            divTitel.textContent = item.name;
        var div = document.createElement("div");
            li.appendChild(div);
            var divArrow = document.createElement("div");
                div.appendChild(divArrow);
                divArrow.classList.add("pfeile-icon");
            var divTag = document.createElement("div");
                div.appendChild(divTag);
                divTag.classList.add("grau-text")
                divTag.textContent = item.numOfTags;
            var divOptions = document.createElement("div");
                div.appendChild(divOptions);
                divOptions.classList.add("options-icon");

    // add the element to the list
    document.getElementsByTagName("ul")[0].appendChild(li);

    setListener()

}