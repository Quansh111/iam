/**
 * Created by master on 01.03.16.
 */
var listenersSet;


// a function that reacts to the selection of a list item
// function onListItemSelected(event) {
//     // check in which phase we are
//     if (event.eventPhase == Event.BUBBLING_PHASE) {
//         // a helper function that looks up the target li element of the event
//         function lookupEventTarget(el) {
//             if (el.tagName.toLowerCase() == "li") {
//                 return el;
//             }
//             else if (el.tagName.toLowerCase() == "ul") {
//                 return null;
//             }
//             else if (el.parentNode) {
//                 return lookupEventTarget(el.parentNode);
//             }
//         }
//
//         // lookup the target of the event
//         var eventTarget = lookupEventTarget(event.target);
//         if (eventTarget) {
//             // from the eventTarget, we find out the title of the list item, which is simply the text content of the li element
//         }
//         else {
//         }
//     }
// }

function setListenerActive() {
    var ul = document.getElementById("main");
    ul.classList.add("hide");
    ul.addEventListener("transitionend", fadeoutView);
}

function fadeoutView(event) {
    document.getElementsByTagName("body")[0].classList.toggle("listeners-active");
    var ul = event.target;
    ul.removeEventListener("transitionend", fadeoutView);
    ul.classList.remove("hide");
}


function reload() {
    var ul = document.getElementsByTagName("ul")[0];
    while (ul.firstChild) {
        ul.removeChild(ul.firstChild);
    }
    loadNewItems()


}

function toggleListeners() {

    //
    // // we set an onclick listener on the list view and check from which item the event was generated
    // // we also set a listener on the '+'-button that loads content from the server!
    // var ul = document.getElementsByTagName("ul")[0];
    // var newView = document.querySelector(".plus-icon")
    // document.getElementsByTagName("body")[0].classList.toggle("listeners-active");
    //
    //
    //
    // if (listenersSet) {
    //     newView.removeEventListener("click",loadNewItems);
    //     ul.removeEventListener("click", onListItemSelected);
    //     listenersSet = false;
    //
    // }
    // else {
    //     newView.addEventListener("click",loadNewItems);
    //     ul.addEventListener("click", onListItemSelected);
    //     listenersSet = true;
    //
    //
    // }
}

/* show a toast and use a listener for transitionend for fading out */
function showToast(msg) {
    // var toast = document.querySelector(".toast");
    // if (toast.classList.contains("active")) {
    //     console.info("will not show toast msg " + msg + ". Toast is currently active, and no toast buffering has been implemented so far...");
    // }
    // else {
    //     console.log("showToast(): " + msg);
    //     toast.textContent = msg;
    //     /* cleanup */
    //     toast.removeEventListener("transitionend",finaliseToast);
    //     /* initiate fading out the toast when the transition has finished nach Abschluss der Transition */
    //     toast.addEventListener("transitionend", fadeoutToast);
    //     toast.classList.add("shown");
    //     toast.classList.add("active");
    // }
}


//
// /* trigger fading out the toast and remove the event listener  */
// function fadeoutToast(event) {
//     var toast = event.target;
//     console.log("fadeoutToast(): " + toast.textContent);
//     /* remove tranistionend listener */
//     toast.addEventListener("transitionend", finaliseToast);
//     toast.removeEventListener("transitionend", fadeoutToast);
//     toast.classList.remove("shown");
// }

var showName = function (event) {
    var imgList = this.querySelector('.img-list');
    var imgUrl = getComputedStyle(imgList).getPropertyValue('background-image');
    var ul = document.getElementsByTagName("ul")[0];

    if (event.target.matches(".options-icon")) {
        var del = confirm(this.querySelector('.bild-titel').innerHTML + "\n" + imgUrl.substring(5, imgUrl.length - 2));
        if (del) {
            ul.removeChild(this.closest('li'));
        }
    } else {
        alert(this.querySelector('.bild-titel').innerHTML);
    }

}

function setListener() {
    var items = document.getElementsByClassName('grid-container');
    for (var x = 0; x < items.length; x++) {
        items[x].addEventListener('click', showName);
    }
}


document.addEventListener("DOMContentLoaded", function () {
    var newItem = document.querySelector(".plus-icon");
    newItem.addEventListener("click", loadNewItem);

    var reloadButton = document.getElementById("refresh-icon");
    reloadButton.addEventListener("click", reload)

    document.getElementsByTagName("body")[0].classList.add("show");
    loadNewItems()
});
